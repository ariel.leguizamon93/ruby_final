class PatientsController < ApplicationController
    def index
      @patients = Patient.all
    end

    def show
      @patient = Patient.find(params[:id])
    end

    def new
      @patient = Patient.new
    end
    def create
      @patient = Patient.new(patient_params)
      if @patient.save
        redirect_to(:action => 'index')
      else
        render('new')
      end
    end

    def edit
      @patient = Patient.find(params[:id])
    end
    def update
      @patient = Patient.find(params[:id])
      if @patient.update_attributes(patient_params)
        redirect_to(:action => 'show', :id => @patient.id)
      else
        render ('edit')
      end
    end

    def delete
      @patient = Patient.find(params[:id])
    end
    def destroy
      Patient.find(params[:id]).destroy
      redirect_to(:action => 'index')
    end
    private
    def patient_params
      params.require(:patient).permit(:name,:surname,:dni,:age,:mail,:password)
    end
end