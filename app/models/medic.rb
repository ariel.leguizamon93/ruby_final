class Medic < ActiveRecord::Base
  belongs_to :office
  has_many :turns
  validates :license_number, uniqueness: true

end
