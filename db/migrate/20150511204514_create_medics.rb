class CreateMedics < ActiveRecord::Migration
  def change
    create_table :medics do |t|
      t.string :name
      t.string :surname
      t.integer :license_number
      t.string :specialization
      t.string :doctor_office
      t.timestamps null: false
    end
  end
end
