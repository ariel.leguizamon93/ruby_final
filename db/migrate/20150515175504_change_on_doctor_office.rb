class ChangeOnDoctorOffice < ActiveRecord::Migration
  def change
    remove_column(:medics,:doctor_office)
    add_reference :medics ,:office, index: true, foreign_key: true
  end
end
